import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import './Find.css';
class Find extends Component{
    render(){
        return(
        <div> 
        {/* <TextField 
            id="outlined-bare" 
            className="{classes.TextField}"
            defaultValue="Bare"
            margin="normal"variant="outlined">
        </TextField> */}

        <TextField>
         <Input
        id="outlined-search"
        label="Tìm Kiếm Sản Phẩm"
        fullWidth
        type="search"
        className="{classes.TextField}"
        margin="normal"
        variant="outlined"
        inputProps={{
            'aria-label': 'Description',
          }}
          onChange = {(e) => props.product(e.target.value)}
        />
  
        </TextField>
      
        </div>
        );
    }
}
export default Find;    