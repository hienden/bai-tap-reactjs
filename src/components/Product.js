import React, {Component} from 'react';
import './Product.css';
class Product extends Component{
    // hienthi(text){
    //     alert(text);
    //     console.log(text);
    // }
  /*  constructor(props){ 
    super(props);
    
    this.hienthi=this.hienthi.bind(this);
    }
    hienthi(){
        alert(this.props.name +   '-'  +   this.props.price);      
    }*/

    // constructor(props){ 
    //     super(props);
    // }
    hienthi = () => {
        alert(this.props.name +   '-'  +   this.props.price);      
    }

  render() {
    return (
  
        <div className="col-lg-6 anh">
            <div className="thumbnail noidung">
                <img alt={this.props.name} src={this.props.img} />
                <div className="caption">
                    <h4>{this.props.name}</h4>
                    <p>{this.props.price}</p>
                    <p>
                        {/* <a href="#top" className="btn btn-primary" onClick={() => {this.hienthi(this.props.name)}}>Mua hàng </a> */}
                        <a href="#top" className="btn btn-primary" onClick= {this.hienthi}>Mua hàng </a>
                      
                    </p>
                </div>
            </div>
        </div>
        
    );
  }
}

export default Product;