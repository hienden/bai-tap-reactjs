import React, {Component} from 'react';

class Header extends Component{
    render(){
        return(
        <div> 
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
              
                         <a  href="#top" className="navbar-brand">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li><a  href="#top">Iphone</a></li>
                        <li><a  href="#top">Sam Sung</a></li>
                        <li  className="active"><a  href="#top">Nokia</a></li>
                        <li><a  href="#top">Oppo</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        );
    }
}
export default Header;    