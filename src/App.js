import React, {Component} from 'react';
import './App.css';
import Header from './components/Header';
import Product from './components/Product';
//import Find from './components/Find';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
//import TitlebarGridList from './TitlebarGridList.js'


function Inputs(props) {
//    const classes = useStyles();
    return (
      <Input
          placeholder="Search Text"
          className='{classes.input}'
          inputProps={{
            'aria-label': 'Description',
          }}
          onChange = {(e) => props.Products(e.target.value)}
        />
    )
  }

class App extends Component {
    constructor(props){ 
        super(props);
        this.onHandleChange  = this.onHandleChange .bind(this)
        
        this.state={
            products : [
                {
                    id:1,
                    name:'Iphone 7',
                    price: 15000000,
                    img:"https://cdn.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s7-2-400x460.png",
                    status: true
                },
                {
                    id:2,
                    name:'Oppo F1s',
                    price: 8000000,
                    img:"https://cdn.tgdd.vn/Products/Images/42/179736/huawei-mate-20-black-400x460.png",
                    status: false
                },
                {
                    id:3,
                    name:'Sam sung',
                    price: 10000000,
                    img:"https://cdn.tgdd.vn/Products/Images/42/74113/samsung-galaxy-s7-2-400x460.png",
                    status: true
                },
                {
                    id:4,
                    name:'huawei',
                    price: 4000000,
                    img:"https://cdn.tgdd.vn/Products/Images/42/179736/huawei-mate-20-black-400x460.png",
                    status: false
                }
            ],

        };
    }
  
    onHandleChange  = (e) => {
        //const { Data } = this.state
        var list = []
        this.state.products.forEach(function(item) {
          if(item.name.toLowerCase().includes(e.toLowerCase())){
            list.push(item)
          }
        })
        this.setState({
          products: list
        })
        
      }
  
    render(){
  let elements = this.state.products.map((product, index) =>{
      let result='';
      if(product.status){
       result= <Product key={product.id}
            price={product.price}
            name={product.name}
            img={product.img}
            >
       </Product>
      }
      return result;
  });
  return (
    <div className="container">
        <div className="Header">
            <Header />
        </div>    
        {/* <div>
        <TextField
        id="outlined-search"
        label="Tìm Kiếm Sản Phẩm"
        fullWidth
        type="search"
        className="{classes.TextField}"
        margin="normal"
        variant="outlined"></TextField>
      
        </div> */}
        <div className="row">
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3 " >
               {elements}
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                {elements}
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                {elements}
            </div>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3" >
                {elements}
            </div>               
         </div>     
         <div>
         <Inputs onHandleChange = {this.onHandleChange}/>
         </div>
         {/* <div>
         <TitlebarGridList products = {this.state.products} />
         </div> */}

    </div>
  );
}
}
export default App;
